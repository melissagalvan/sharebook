import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'

Vue.use(Vuex)

const apiUrl = 'http://localhost:8000/'

export default new Vuex.Store({
  state: {
    comments: []
  },
  mutations: {
    ADD_COMMENT(state, comment) {
      state.comments.push(comment);
    },
    MODIFY_COMMENTS(state, comments) {
      state.comments = comments;
    },
  },
  actions: {
    async addComment({commit}, {comment, routeParam }) {      
      let response = await Axios.post(apiUrl + 'add-comment/' +  routeParam, comment);
      commit('ADD_COMMENT', response.data);
    },
    async fetchComments({ commit }) {
      let response = await Axios.get(apiUrl + 'comments/');
      commit('MODIFY_COMMENTS', response.data);
    }
  },
  modules: {
  }
})
